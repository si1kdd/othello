#include "logic.h"

int set_server(int port)
{
        struct sockaddr_in serv_addr, clie_addr;
        memset(&serv_addr, 0, sizeof(serv_addr));
        serv_addr.sin_family            = AF_INET;
        serv_addr.sin_port              = htons(port);
        serv_addr.sin_addr.s_addr       = htonl(INADDR_ANY);

        /* socket -> bind -> listen -> accept */
        int serv_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
        if (serv_fd < 0) {
                perror("[!] Server socket() Error\n");
                exit(errno);
        }
        int optval = 1;
        setsockopt(serv_fd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

        if (bind(serv_fd, (struct sockaddr *)&serv_addr,
                 sizeof(struct sockaddr_in)) < 0) {
                perror("[!] Server bind() Error\n");
                exit(errno);
        }
        if (listen(serv_fd, 25) < 0) {
                perror("[!] Server listen Error\n");
                exit(errno);
        }
        fprintf(stderr, "You are listen on %d\n", port);
        socklen_t clilen = sizeof(clie_addr);
        int clie_fd = accept(serv_fd, (struct sockaddr *)&clie_addr, &clilen);
        if (clie_fd < 0) {
                perror("[!] Server accept() fail!!\n");
                exit(errno);
        }

        me = PLAYER1; opponent = PLAYER2;
        is_myturn = 1;
        return clie_fd;
}

int set_client(char *ip_addr)
{
        // parsing.
        char *saveptr   = NULL;
        char *ip        = NULL;
        char *port      = NULL;
        char *dup_str   = strdup(ip_addr);

        int c = 0;
        char *sub_str = strtok_r(dup_str, ":", &saveptr);
        while (sub_str != NULL) {
                if (c == 0)
                        ip = strdup(sub_str);
                else
                        port = strdup(sub_str);
                sub_str = strtok_r(NULL, ":", &saveptr);
                c++;
        }
        if (c == 0) {
                perror("[!] Wrong Input format!!\n");
                exit(-1);
        }

#ifdef DEBUG
        printf("%s %s", ip, port);
#endif
        free(dup_str);

        struct addrinfo hints, *res;
        memset(&hints, 0, sizeof(hints));
        hints.ai_family         = AF_UNSPEC;
        hints.ai_socktype       = SOCK_STREAM;
        /* getaddinfo -> socket -> connect */
        int recv = getaddrinfo(ip, port, &hints, &res);
        if (recv < 0) {
                perror("[!] Client getaddrinfo() Error\n");
                exit(errno);
        }

        int sockfd = -1;
        // for (struct addrinfo p = res; p != NULL; p = p->ai_next) {
                sockfd = socket(AF_INET, SOCK_STREAM, 0);
                if (sockfd < 0) {
                        perror("[!] Client socket() Error\n");
                        exit(errno);
                        // continue;
                }
                if (connect(sockfd, res->ai_addr, res->ai_addrlen) < 0) {
                        perror("[!] Client connect() Failed\n");
                        close(sockfd);
                        exit(errno);
                        // continue;
                }
                // break;
        // }
        // success here.
        freeaddrinfo(res);
        free(ip);
        free(port);

        me = PLAYER2; opponent = PLAYER1;
        is_myturn = 0;
        return sockfd;
}

int is_legal(int x, int y, int who)
{
        int x_direct[] = {1, 0, -1, 0, 1, 1, -1, -1};
        int y_direct[] = {0, 1, 0, -1, -1, 1, 1, -1};

        if (board[y][x] != 0) return 0;
        for (int i = 0; i < 8; ++i)
        {
                int opponent_chess = 0;
                int nx = x, ny = y;
                do {
                        nx += x_direct[i];
                        ny += y_direct[i];
                        opponent_chess++;
                } while(nx >= 0 && ny >= 0 &&
                                nx < BOARD_SIZE && ny < BOARD_SIZE &&
                                board[ny][nx] != 0 && board[ny][nx] != who);

                if (nx >= 0 && nx < BOARD_SIZE && ny >= 0 && ny < BOARD_SIZE
                                && board[ny][nx] == who && opponent_chess > 1)
                        return 1;
        }
        return 0;
}

int still_can_move(int who)
{
        for (int i = 0; i < BOARD_SIZE; ++i)
        {
                for (int j = 0; j < BOARD_SIZE; ++j)
                {
                        if (is_legal(i, j, who) == 1)
                                return 1;
                }
        }
        return 0;
}

void update_board(int x, int y, int my_place)
{
        int x_direct[] = {1, 0, -1, 0, 1, 1, -1, -1};
        int y_direct[] = {0, 1, 0, -1, -1, 1, 1, -1};

        for (int i = 0; i < 8; ++i)
        {
                int nx = x, ny = y;
                int opponent_chess = 0;
                do {
                        nx += x_direct[i]; ny += y_direct[i];
                        opponent_chess++;

                } while (nx >= 0 && nx < BOARD_SIZE && ny >= 0 && ny < BOARD_SIZE && board[ny][nx] != 0 && board[ny][nx] != my_place);

                if (nx >= 0 && nx < BOARD_SIZE && ny >= 0 && ny < BOARD_SIZE
                                && board[ny][nx] != 0 && board[ny][nx] == my_place && opponent_chess > 1) {
                        while (nx != x || ny != y)
                        {
                                board[ny][nx] = my_place;
                                ny -= y_direct[i]; nx -= x_direct[i];
                        }
                }
        }
}

void draw_game_status()
{
        char msgs[256];
        int player = (me == PLAYER1) ? 1 : 2;
        if (is_myturn) {
                snprintf(msgs, sizeof(msgs),
                                "Player #%d: It's my turn !", player);
                draw_message(msgs, 0);
        }
        else {
                snprintf(msgs, sizeof(msgs),
                                "Player #%d: Waiting for peer.....", player);
                draw_message(msgs, 1);
        }
        return;
}

void exchange_turn()
{
        is_myturn = !is_myturn;
        draw_game_status();
        refresh();
}

void send_coordinate(int sockfd)
{
        char send_msg[20] = {0};
        sprintf(send_msg, "%d %d", cx, cy);
        write(sockfd, send_msg, sizeof(send_msg));
}

void user_io(int sockfd, int *is_moved)
{
        int ch = getch();
        switch (ch)
        {
                case ' ': case 0x0d: case 0x0a: case KEY_ENTER:
                        if (is_myturn) {
                                if (is_legal(cx, cy, me) == 0) {
                                        draw_message("No! Invalid place", 1);
                                        break;
                                }
                                if (board[cy][cx] != 0)
                                        break;

                                set_board(cx, cy, me);
                                exchange_turn();
                                is_moved++;
                                send_coordinate(sockfd);
                        }
                        break;
                case 'q': case 'Q':
                        game_quit(sockfd, 1);
                        break;
                case 'k': case KEY_UP:
                        draw_cursor(cx, cy, 0);
                        cy = (cy - 1 + BOARD_SIZE) % BOARD_SIZE;
                        draw_cursor(cx, cy, 1);
                        is_moved++;
                        break;
                case 'j': case KEY_DOWN:
                        draw_cursor(cx, cy, 0);
                        cy = (cy + 1 ) % BOARD_SIZE;
                        draw_cursor(cx, cy, 1);
                        is_moved++;
                        break;
                case 'h': case KEY_LEFT:
                        draw_cursor(cx, cy, 0);
                        cx = (cx - 1 + BOARD_SIZE) % BOARD_SIZE;
                        draw_cursor(cx, cy, 1);
                        is_moved++;
                        break;
                case 'l': case KEY_RIGHT:
                        draw_cursor(cx, cy, 0);
                        cx = (cx + 1) % BOARD_SIZE;
                        draw_cursor(cx, cy, 1);
                        is_moved++;
                        break;
                default:
                        break;
        }
        if (is_moved) {
                refresh();
                is_moved = 0;
        }
        napms(1);
}

void find_winner()
{
        int my_point = 0, opponent_point = 0;
        for (int i = 0; i < BOARD_SIZE; ++i)
        {
                for (int j = 0; j < BOARD_SIZE; ++j)
                {
                        if (board[i][j] == me)
                                my_point++;
                        if (board[i][j] == opponent)
                                opponent_point++;
                }
        }

        if (my_point > opponent_point)
                draw_message("You WIN!!", 0);
        else if (my_point < opponent_point)
                draw_message("You LOSE......", 1);
        else
                draw_message("Game Draw!", 0);

        refresh();

        while(1) {
                int ch = getch();
                if (ch == 'q' || ch == 'Q')
                        break;
        }
        refresh();

        return;
}

void game_quit(int sockfd, int status)
{
        char *exit_msg = NULL;
        switch(status)
        {
                case 0:
                        exit_msg = "[!] Game Over !";
                        break;
                case 1:
                        exit_msg = "[!] User Exit !";
                        break;
                case 2:
                        exit_msg = "[!] Error exit\n";
                        find_winner();
                        break;
                case 3:
                        exit_msg = "[!] Opponent Exit !";
                        break;
        }

        endwin();
        printf("%s\n", exit_msg);
        close(sockfd);
        exit(0);
}

void set_board(int x, int y, int who)
{
        board[y][x] = who;
        update_board(x, y, who);
        draw_board(); draw_score();
}

void get_opponent_msg(int sockfd)
{
        char recv_msg[20] = {0};
        int recv_len = 0;
repeat:
        recv_len = read(sockfd, recv_msg, sizeof(recv_msg));
        if (recv_len <= 0) {
                if (recv_len == 0) {
                        draw_message("[!] Can't get opponent info, opponent close the connection.", 1);
                        game_quit(sockfd, 3);
                }
                else if (errno == EINTR)
                        goto repeat;
        }

        int ox, oy;
        sscanf(recv_msg, "%d %d", &ox, &oy);

        if (ox == -1 || oy == -1) {
                draw_message("[!] Opponent can't move anymore, Just stop this game", 0);
                find_winner();
                write(sockfd, "-1 -1", 6);
                game_quit(sockfd, 0);
        }
        else if (ox >=0 && oy >= 0 && ox < BOARD_SIZE && oy < BOARD_SIZE) {
                set_board(ox, oy, opponent);
                exchange_turn();

                if (still_can_move(me) == 0) {
                        if (still_can_move(opponent) == 0) {
                                find_winner();
                                game_quit(sockfd, 0);
                        }
                        else {
                                draw_message("No more valid move now... Please press p to over this game.", 0);
                                refresh();
                                while(1) {
                                        int ch = getch();
                                        if (ch == 'p' || ch == 'P')
                                                break;
                                }
                                is_myturn = !is_myturn;
                                write(sockfd, "-1 -1", 6);
                                find_winner();
                        }
                        write(sockfd, "-1 -1", 6);
                }
                else if (still_can_move(opponent) == 0) {
                        find_winner();
                        game_quit(sockfd, 0);
                }
                refresh();
        }
}

void game_loop(int sockfd)
{
        fd_set read_set;
        FD_ZERO(&read_set);
        game_over = 0;
        while (1)
        {
                int is_moved = 0;
                FD_SET(0, &read_set); FD_SET(sockfd, &read_set);
                struct timeval tv;
                tv.tv_sec = 0; tv.tv_usec = 0;
                int rs = select(sockfd + 1, &read_set, NULL, NULL, &tv);
                if (rs == -1)
                        perror("[!] select() Error\n");

                if (FD_ISSET(sockfd, &read_set)) {
                        get_opponent_msg(sockfd);
                }
                if (FD_ISSET(0, &read_set)) {
                        user_io(sockfd, &is_moved);
                }
        }
}

void init_screen()
{
        initscr();
        getmaxyx(stdscr, height, width);
        cbreak();
        halfdelay(1);
        noecho();
        keypad(stdscr, TRUE);
        curs_set(0);

        init_colors();
        clear();
        cx = cy = 3;
        init_board();
        draw_board(); draw_cursor(cx, cy, 1); draw_score();
        draw_game_status();

        attron(A_BOLD);
        move(height - 1, 0);
        printw("Arrow keys: move; Space/Enter: put; Q: quit");
        attroff(A_BOLD);
        refresh();
}

void game_start(int sockfd)
{
        init_screen();
        game_loop(sockfd);
}
