#include "logic.h"

int server = 0, client = 0;

void print_help()
{
        printf("Server Mode: ./othello -s <port>\n");
        printf("Client Mode: ./othello -c [hostname | ip address]:<port>\n");
        printf("\t Example: ./othello -c 172.168.1.5:8888");
        return;
}

int main(int argc, char *argv[])
{
        if (argc < 3) {
                print_help();
                exit(-1);
        }
        unsigned int port_number = 0;

        int sockfd = -1;
        if (!strcmp(argv[1], "-s") && argc == 3) {
                port_number = atoi(argv[2]);
                if (!isdigit(port_number)) {
                        // calling.
                        server = 1;
                        sockfd = set_server(port_number);
                } else {
                        perror("Please input the correct port number!!\n");
                        exit(-1);
                }
        } else if (!strcmp(argv[1], "-c") && argc == 3) {
                // don't checking here, just calling.
                client = 1;
                sockfd = set_client(argv[2]);
        } else {
                perror("[!] Wrong Input\n");
                print_help();
                exit(-1);
        }

        if (sockfd != -1 && (client || server)) {
                game_start(sockfd);
        }
        else {
                printf("[!] Not game start !\n");
        }
        return 0;
}
