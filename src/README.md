### Othello game (using ncurse)

### Features:
* Your program can act as either a server (player #1) or a client (player #2) by using the respective command arguments.
* A server has to wait for a client connection.
* A client can connect to the given server (IP address or host name).
* Once connected, display the game board. The game always starts from player #1 (server).
* Player can only put pieces (discs) on valid places (see game rule).
* Display correct number of pieces on the game board for the both players.
* Implement the rest of game logics.*
* When there is no more moves, display a message to show the player wins or loses.
* Ensure the both two players have the same view of game board. If either the client or the server quits, the peer has to be terminated as well.

