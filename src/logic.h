#ifndef __LOGIC_H__
#define __LOGIC_H__

#include <sys/socket.h>
#include <sys/types.h>
// #include <sys/epoll.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>

#include "othello.h"

int me, opponent, is_myturn;
int game_over;
int cx, cy;
int height, width;

int set_server(int port);
int set_client(char *ip_addr);

int is_legal(int x, int y, int who);
int still_can_move(int who);

void update_board(int x, int y, int my_place);
void draw_game_status();
void exchange_turn();
void send_coordinate(int sockfd);
void user_io(int sockfd, int *is_moved);
void find_winner();

void game_quit(int sockfd, int status);
void set_board(int x, int y, int who);
void get_opponent_msg(int sockfd);
void game_loop(int sockfd);

void init_screen();
void game_start(int sockfd);

#endif /* __LOGIC_H__ */
