# Othello
* A small networking othello game. (Homework of NCTU Unix Programming lesson)

- - -

* Version (Beta):
  - synchronous I/O version. (linux select API)

* TODO:
  - epoll API version.
  - using libevent.
  - asynchronous I/O version.
